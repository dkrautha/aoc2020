struct Record {
    required_char: char,
    min_count: usize,
    max_count: usize,
    password: String,
}

impl Record {
    fn is_valid(&self) -> bool {
        let mut count = 0;
        for c in self.password.chars() {
            if c == self.required_char {
                count += 1;
            }
        }
        (count >= self.min_count) && (count <= self.max_count)
    }

    fn is_valid_again(&self) -> bool {
        let chars: Vec<char> = self.password.chars().collect();
        let index1 = chars[self.min_count - 1] == self.required_char;
        let index2 = chars[self.max_count - 1] == self.required_char;
        (index1 || index2) && (!(index1 && index2))
    }
}

fn get_records(filename: &str) -> Vec<Record> {
    let mut records = Vec::new();
    if let Ok(lines) = crate::common::read_lines(filename) {
        lines.for_each(|line| {
            if let Ok(result) = line {
                let split_result: Vec<&str> =
                    result.split(|c| c == '-' || c == ' ' || c == ':').collect();
                records.push(Record {
                    required_char: (split_result[2].parse().unwrap()),
                    min_count: (split_result[0].parse().unwrap()),
                    max_count: (split_result[1].parse().unwrap()),
                    password: (split_result[4]).parse().unwrap(),
                });
            }
        })
    }
    records
}

pub fn part_1_and_2() {
    let file_path = "inputs/day_02.txt";
    let records = get_records(file_path);

    let mut count1 = 0;
    let mut count2 = 0;

    for record in records {
        if record.is_valid() {
            count1 += 1;
        }
        if record.is_valid_again() {
            count2 += 1;
        }
    }

    println!("{} valid records by first ruleset", count1);
    println!("{} valid records by second ruleset", count2);
}
