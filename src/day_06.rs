use std::{collections::HashSet, fs};

pub fn part1() {
    let filename = "inputs/day_06.txt";
    let file_as_string = fs::read_to_string(filename).expect("failed to read file");

    let answer: usize = file_as_string
        .split("\n\n")
        .map(|block| {
            block
                .chars()
                .filter(|c| !c.is_whitespace())
                .collect::<HashSet<_>>()
                .len()
        })
        .sum();

    println!("Total Count: {}", answer);
}

pub fn part2() {
    let filename = "inputs/day_06.txt";
    let file_as_string = fs::read_to_string(filename).unwrap_or_default();

    let answer: usize = file_as_string
        .split("\n\n")
        .map(|block| {
            block
                .lines()
                .map(|line| -> HashSet<char> { line.chars().collect() })
                .reduce(|a, b| &a & &b)
                .unwrap_or_default()
                .len()
        })
        .sum();

    println!("Total Count: {}", answer);
}

#[allow(dead_code)]
fn load_groups(filename: &str) -> Vec<Vec<String>> {
    let mut groups = Vec::new();
    groups.push(Vec::new());

    if let Ok(lines) = crate::common::read_lines(filename) {
        for line in lines {
            if let Ok(line) = line {
                if line.len() == 0 {
                    groups.push(Vec::new());
                } else {
                    groups.last_mut().unwrap().push(line);
                }
            }
        }
    }
    groups
}

// example not loading the entire string in at once
#[allow(dead_code)]
fn part1_other() {
    let groups = load_groups("inputs/day_06.txt");

    let answer: usize = groups
        .iter()
        .map(|block| {
            block
                .iter()
                .map(|line| -> HashSet<char> { line.chars().collect() })
                .reduce(|a, b| a.union(&b).cloned().collect())
                .unwrap_or_default()
                .len()
        })
        .sum();
    println!("answer: {}", answer);
}
