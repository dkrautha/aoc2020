#[derive(Debug)]
struct Seat {
    row: i32,
    column: i32,
    id: i32,
}

impl Seat {
    fn from_str(s: &str) -> Self {
        let mut row = 127;
        let mut row_delta = 128 / 2;
        let mut column = 7;
        let mut column_delta = 8 / 2;
        for c in s.chars() {
            match c {
                'F' => {
                    row -= row_delta;
                    row_delta /= 2;
                }
                'B' => {
                    row_delta /= 2;
                }
                'L' => {
                    column -= column_delta;
                    column_delta /= 2;
                }
                'R' => {
                    column_delta /= 2;
                }
                _ => panic!("oh god its going down for real"),
            }
        }

        Seat {
            row,
            column,
            id: row * 8 + column,
        }
    }

    // After completing the solution once I realized the letters could be
    // thought of as bits of a binary number, and this is an attempt at thinking
    // that way. I didn't test this extensively, but it seems to generally be
    // slower than the other method unless I figure out a way to do this more
    // efficiently (I would be baffled if there wasn't);
    #[allow(dead_code)]
    fn from_str_alt(s: &str) -> Self {
        let row = s
            .chars()
            .take(7)
            .map(|c| match c {
                'B' => true,
                'F' => false,
                _ => unreachable!(),
            })
            .enumerate()
            .map(|(i, val)| if val { 2_i32.pow(6 - i as u32) } else { 0 })
            .sum();

        let column = s
            .chars()
            .skip(7)
            .take(3)
            .map(|c| match c {
                'R' => true,
                'L' => false,
                _ => unreachable!(),
            })
            .enumerate()
            .map(|(i, val)| if val { 2_i32.pow(2 - i as u32) } else { 0 })
            .sum();

        Seat {
            row,
            column,
            id: row * 8 + column,
        }
    }
}

fn load_seats(filename: &str) -> Vec<Seat> {
    let mut seats = Vec::new();

    if let Ok(lines) = crate::common::read_lines(filename) {
        for line in lines {
            if let Ok(s) = line {
                seats.push(Seat::from_str(&s));
            }
        }
    }

    seats
}

pub fn part_1_and_2() {
    let seats = load_seats("inputs/day_05.txt");

    /*
     * testing load seats with the different from_str functions
    for _ in 0..1000 {
        seats = load_seats("inputs/day_05.txt");
    }
    */

    let mut ids = seats.iter().map(|s| s.id).collect::<Vec<i32>>();
    ids.sort_unstable();

    let max = ids.last();

    if let Some(m) = max {
        println!("Highest ID: {}", m);
    }

    for (first, second) in ids.windows(2).map(|pair| {
        if let &[first, second] = pair {
            (first, second)
        } else {
            unreachable!();
        }
    }) {
        if second - first != 1 {
            println!("My ID: {}", second - 1);
            break;
        }
    }
}
